

FIXED POINT INTEGER SECRET SHARING FROM REAL-VALUED INPUT FILES
___________________________________________________________________

(1) Open input_gen.py
(2) On line 68
	(a) adjust the decimal accuracy to the desired value for training the model
	(b) note: this must be consistent with the values specified in the .toml files
(3) On line 70
	(a) adjust in_path to the directory containing training the training files to secret share
	(b) these files must be .csv's 
	(c) they must contain the word 'train' in the filename. if they do not:
		(i) Replace line 77 with 'if filename.endswith('.csv'):'
(4) On line 71
	(a) change output_path to the name of the directory the computing parties use as inputs in their .toml files
(5) From the terminal
	(a) run the code with 'python input_gen.py'
	(b) if successful, a message will be displayed showing where the output files are located