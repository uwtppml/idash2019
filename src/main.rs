extern crate idash2019_rust;
use idash2019_rust::computing_party::*;
use idash2019_rust::trusted_initializer::*;
use std::env;
use std::time::{SystemTime};
//use std::alloc::System;
// #[global_allocator]
// static GLOBAL: System = System;

fn main() {
	
  let prefix = "main:      ";

  println!("{} runtime count starting...", &prefix);
  let now = SystemTime::now();
  let args: Vec<String> = env::args().collect();
  let settings_file = args[1].clone();

	let mut settings = config::Config::default();
    settings
        .merge(config::File::with_name( &settings_file.as_str() )).unwrap()
        .merge(config::Environment::with_prefix("APP")).unwrap();

    match settings.get_bool("ti") {
    	Ok(is_ti) => {
    		if is_ti {
    			trusted_initializer::ti_module( settings_file.clone() );
    		} else {
          computing_party::party_module( settings_file.clone() ); 		
    		}
    	},
    	Err(error) => {
    		panic!(
          format!("{} encountered a problem while parsing settings: {:?}", &prefix, error))
    	},
    };

    println!("{} total runtime = {:9} (ms)", &prefix, now.elapsed().unwrap().as_millis());

}
