/* Party0 and Party1 connect to one another and to the 
Trusted Initializer. They train then train a secure MPC logistic
regression model on a secret-shared, distributed data set and 
output weights in the clear
*/

pub mod computing_party {

	use crate::init::*;
	use crate::logistic_regression::*;
	use std::time::SystemTime;

	pub fn party_module( settings_file : String ) {

		let prefix = "main:      ";

		println!("{} initializing runtime context...", &prefix);
		let now = SystemTime::now();

		let mut ctx = init::initialize_runtime_context( settings_file );
		
		println!("{} runtime context initialized -- work time = {:5} (ms)", 
			&prefix, now.elapsed().unwrap().as_millis() );

	   	logistic_regression::lr_module(&mut ctx);
	}

}
