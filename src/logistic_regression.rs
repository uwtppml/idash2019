pub mod logistic_regression {

//extern crate csv;

use crate::init::*;
use crate::protocol::*;
use crate::utility::*;
use crate::constants::*;
use std::num::Wrapping;
use std::fs::File;
use std::time::{SystemTime};
use std::net::TcpStream;
use std::io::{Read, Write};

const TI_BATCH_SIZE: usize = constants::TI_BATCH_SIZE;
const U64S_PER_TX: usize = constants::U64S_PER_TX;
const U8S_PER_TX: usize = constants::U8S_PER_TX;

union Xbuffer {
	u64_buf : [ u64 ; U64S_PER_TX ],
	u8_buf  : [ u8 ;  U8S_PER_TX ],
} 


pub fn lr_module( ctx: &mut init::Context ) {

	let prefix = "lr module: ";

	let debug 		    = ctx.debug_output;
	let attribute_count = ctx.attribute_count;
	let instance_count  = ctx.instance_count;
	let iterations      = ctx.iterations;

	let x_matrix = ctx.x_matrix.clone();
	let y_matrix = ctx.y_matrix.clone();

	let mut weights = vec![ Wrapping(0u64) ; attribute_count ];

	let mut i = 0;
	while i < iterations {

		/* receive correlated randomness from ti */
		print!("{} [{}] receiving correlated randomness... ", &prefix, i);
		let now = SystemTime::now();
		let cr = ti_receive( 
			ctx.ti_stream.try_clone().expect("failed to clone ti recvr"),
			ctx.add_shares_per_iter, 
			ctx.xor_shares_per_iter ); 
		ctx.corr_rand = cr.0;
		ctx.corr_rand_xor = cr.1;
		println!("complete -- work time = {:5} (ms)", 
			now.elapsed().unwrap().as_millis() );

		/* compute weights dot instances */
		print!("{} [{}] computing dot products...          ", &prefix, i);
		let now = SystemTime::now();
		let mut z_list = vec![Wrapping(0u64) ; instance_count ];
		for k in 0..instance_count {
			z_list[k] = protocol::dot_product( 
				&x_matrix[k], &weights, ctx, ctx.decimal_precision, true, false);
		}	
		println!("complete -- work time = {:5} (ms)", 
			now.elapsed().unwrap().as_millis() );

		/* get activation of each instance */
		let now = SystemTime::now();
		print!("{} [{}] computing activations...           ", &prefix, i);
		let o_list = batch_activate(&z_list, ctx);	
		println!("complete -- work time = {:5} (ms)", 
			now.elapsed().unwrap().as_millis() );

		/* calculate sum of gradients for each instance */
		let now = SystemTime::now();
		print!("{} [{}] computing sum of gradients...      ", &prefix, i);
		let mut current_sum = vec![Wrapping(0u64) ; attribute_count];
		for k in 0..instance_count {
			let diff_list =  vec![y_matrix[k][0] - o_list[k] ; attribute_count];
			let gradient = protocol::batch_multiply(&x_matrix[k], &diff_list, ctx);

			for j in 0..attribute_count {
				current_sum[j] += gradient[j];
			}
		}
		for j in 0..attribute_count {
			current_sum[j] = utility::truncate_local(
				current_sum[j], ctx.decimal_precision, (*ctx).asymmetric_bit);
		}
		println!("complete -- work time = {:5} (ms)", 
			now.elapsed().unwrap().as_millis() );
		
		/* update weights */
		let mut scaled_sum = vec![Wrapping(0u64) ; attribute_count];
		for k in 0..attribute_count {
			scaled_sum[k] = utility::truncate_local(
				ctx.learning_rate * current_sum[k], ctx.decimal_precision, (*ctx).asymmetric_bit);
			weights[k] += scaled_sum[k];
		}
		i += 1;
	}

	/* reveal weights and write to outfile */
	let weight_revealed = protocol::reveal(&weights, ctx, ctx.decimal_precision, true, false);
	generate_weights_file(&weight_revealed, &(*ctx).output_path);
	println!("{} training complete -- weights written to: {}.", 
		&prefix, (*ctx).output_path);
}

fn batch_activate( z_list : &Vec<Wrapping<u64>>, 
				   ctx    : &mut init::Context) -> Vec<Wrapping<u64>> {

	let frac_bitmask : u64 = (1 << (ctx.decimal_precision - 1)) - 1;
	let int_bitmask  : u64 = (1 << (ctx.integer_precision + 1)) - 1;

	let one_half: Wrapping<u64> = Wrapping(1 << (ctx.decimal_precision - 1));

	let len            = z_list.len();//ctx.instance_count;
	let asymmetric_bit = (*ctx).asymmetric_bit as u64;
	let inversion_mask: u64 = ( - Wrapping(asymmetric_bit) ).0;

	let z_decomp_list  = protocol::batch_bit_decomp(z_list, ctx); 

	let mut msb_list      = vec![ 0u64 ; len ];
	let mut not_msb_list  = vec![ 0u64 ; len ];
	let mut z_neg_list    = vec![ Wrapping(0) ; len ];

	for i in 0..len {

		msb_list[i]     = z_decomp_list[i] >> 63;
		not_msb_list[i] = (z_decomp_list[i] >> 63) ^ asymmetric_bit;
		z_neg_list[i]   = -z_list[i];
	}

	let msb_list     = protocol::binary_vector_to_ring( &msb_list, ctx );
	let not_msb_list = protocol::binary_vector_to_ring( &not_msb_list, ctx );

	let z_unchanged = protocol::batch_multiply( &not_msb_list, &z_list, ctx );
	let z_flipped   = protocol::batch_multiply( &msb_list, &z_neg_list, ctx );
	
	let mut z_rectified = vec![ Wrapping(0u64) ; len ];

	for i in 0..len {

		z_rectified[i] = z_unchanged[i] + z_flipped[i];
	}

	let z_rectified_decomp = protocol::batch_bit_decomp(&z_rectified, ctx);

	let mut z_frac_list = vec![ 0u64 ; len ];
	let mut z_int_list  = vec![ 0u64 ; len ];

	for i in 0..len {
		z_frac_list[i] = z_rectified_decomp[i] & frac_bitmask;
		z_int_list[i]  = 
			(z_rectified_decomp[i] >> (ctx.decimal_precision - 1))  & int_bitmask;
	}

	let mut not_or_list = vec![ 0u64 ; len ]; 
	let mut or_list     = vec![ 0u64 ; len ]; 
	
	for i in 0..len {
		not_or_list[i] = (z_int_list[i] ^ inversion_mask) & 1u64;
		z_int_list[i] = (z_int_list[i] ^ inversion_mask) >> 1;
	}

	for _i in 1..ctx.integer_precision {
		not_or_list = protocol::batch_bitwise_and(&not_or_list, &z_int_list, ctx, false);

		for j in 0..len {
			z_int_list[j] >>= 1;
		}
	}

	for i in 0..len {
		not_or_list[i] &= 1u64;
		or_list[i] = (not_or_list[i] ^ inversion_mask) & 1u64;
	}

	let or_list     = protocol::binary_vector_to_ring( &or_list, ctx );
	let not_or_list = protocol::binary_vector_to_ring( &not_or_list, ctx );
	
	let z_frac_list = protocol::xor_share_to_additive( &z_frac_list, ctx, (ctx.decimal_precision-1) as usize );

	let mut const_region = vec![ Wrapping(0u64) ; len ];

	for i in 0..len {
		const_region[i] = one_half * or_list[i];
	}

	let linear_region = protocol::batch_multiply(&z_frac_list, &not_or_list, ctx);

	let mut partial_result_pos = vec![ Wrapping(0) ; len ]; 
	let mut partial_result_neg = vec![ Wrapping(0) ; len ];
	
	for i in 0..len {
		partial_result_pos[i] = const_region[i] + linear_region[i];
		partial_result_neg[i] = - partial_result_pos[i]; 
	}

	let pos_selected = protocol::batch_multiply(&partial_result_pos, &not_msb_list, ctx);
	let neg_selected = protocol::batch_multiply(&partial_result_neg, &msb_list, ctx);

	let mut activations = vec![ Wrapping(0u64) ; len ];

	for i in 0..len {
		activations[i] = pos_selected[i] + neg_selected[i] + (Wrapping(asymmetric_bit) * one_half); // + one_half;
	}

	activations
}

fn generate_weights_file( weights: &Vec<f64>, file_path: &String ) {

	let mut file = File::create(file_path).expect("Unable to create file");
	for elem in weights.iter() {
		write!(file, "{}\n", elem).expect("Failed to output weights");
	}
}


fn ti_receive(mut stream: TcpStream, add_share_cnt: usize, xor_share_cnt: usize) -> 
	(Vec<(Wrapping<u64>, Wrapping<u64>, Wrapping<u64>)>, Vec<(u64, u64, u64)>) {

	stream.set_ttl(std::u32::MAX).expect("set_ttl call failed");
	stream.set_write_timeout(None).expect("set_write_timeout call failed");
	stream.set_read_timeout(None).expect("set_read_timeout call failed");  

	let mut xor_shares: Vec<(u64, u64, u64)> = Vec::new();
	let mut add_shares: Vec<(Wrapping<u64>, Wrapping<u64>, Wrapping<u64>)> = Vec::new();

	let mut recv_buf = [ 0u8 ; 11 ];
	let msg = b"send shares";
	let mut bytes_written = 0;
	while bytes_written < msg.len() {
		let current_bytes = stream.write(&msg[bytes_written..]);
		bytes_written += current_bytes.unwrap();
	}
	
	let mut bytes_read = 0;
	while bytes_read < recv_buf.len() {
		let current_bytes = stream.read( &mut recv_buf[bytes_read..] ).unwrap();
		bytes_read += current_bytes;
	}

	assert_eq!(msg, &recv_buf);

	//////////////////////// RECV ADDITIVES ////////////////////////
	let mut remainder = add_share_cnt;
	while remainder >= TI_BATCH_SIZE {

		let mut rx_buf = Xbuffer { u64_buf : [ 0u64 ; U64S_PER_TX ] };

		let mut bytes_read = 0;
		while bytes_read < U8S_PER_TX {
			let current_bytes = unsafe { 
				stream.read(&mut rx_buf.u8_buf[bytes_read..]) };
			bytes_read += current_bytes.unwrap();
		}

		for i in 0..TI_BATCH_SIZE {

			let u = unsafe{ rx_buf.u64_buf[3*i] };
			let v = unsafe{ rx_buf.u64_buf[3*i+1] };
			let w = unsafe{ rx_buf.u64_buf[3*i+2] };

			//println!("recvd: ({:X},{:X},{:X})", u, v, w);
			add_shares.push( (Wrapping(u), Wrapping(v), Wrapping(w)) );
		}

		remainder -= TI_BATCH_SIZE;
	}

	let mut rx_buf = Xbuffer { u64_buf : [ 0u64 ; U64S_PER_TX ] };

	let mut bytes_read = 0;
	while bytes_read < U8S_PER_TX {
		let current_bytes = unsafe { 
			stream.read(&mut rx_buf.u8_buf[bytes_read..])};
		bytes_read += current_bytes.unwrap();
	}

	for i in 0..remainder {

		let u = unsafe{ rx_buf.u64_buf[3*i] };
		let v = unsafe{ rx_buf.u64_buf[3*i+1] };
		let w = unsafe{ rx_buf.u64_buf[3*i+2] };

		//println!("recvd: ({:X},{:X},{:X})", u, v, w);
		add_shares.push( (Wrapping(u), Wrapping(v), Wrapping(w)) );
	}

	//println!("additive shares recv'd. recving xor shares");

	///////////////// RECV XOR SHARES ///////////////////////////

	let mut remainder = xor_share_cnt;
	while remainder >= TI_BATCH_SIZE {

		let mut rx_buf = Xbuffer { u64_buf : [ 0u64 ; U64S_PER_TX ] };

		let mut bytes_read = 0;
		while bytes_read < U8S_PER_TX {
			let current_bytes = unsafe { 
				stream.read(&mut rx_buf.u8_buf[bytes_read..]) };
			bytes_read += current_bytes.unwrap();
		}

		for i in 0..TI_BATCH_SIZE {

			let u = unsafe{ rx_buf.u64_buf[3*i] };
			let v = unsafe{ rx_buf.u64_buf[3*i+1] };
			let w = unsafe{ rx_buf.u64_buf[3*i+2] };

			//println!("recvd: ({:X},{:X},{:X})", u, v, w);
			xor_shares.push( (u, v, w) );
		}

		remainder -= TI_BATCH_SIZE;
	}

	let mut rx_buf = Xbuffer { u64_buf : [ 0u64 ; U64S_PER_TX ] };

	let mut bytes_read = 0;
	while bytes_read < U8S_PER_TX {
		let current_bytes = unsafe { 
			stream.read(&mut rx_buf.u8_buf[bytes_read..])};
		bytes_read += current_bytes.unwrap();
	}

	for i in 0..remainder {

		let u = unsafe{ rx_buf.u64_buf[3*i] };
		let v = unsafe{ rx_buf.u64_buf[3*i+1] };
		let w = unsafe{ rx_buf.u64_buf[3*i+2] };

		//println!("recvd: ({:X},{:X},{:X})", u, v, w);
		xor_shares.push( (u, v, w) );
	}

	//println!("xor shares recv'd");

	assert_eq!(add_share_cnt, add_shares.len());
	assert_eq!(xor_share_cnt, xor_shares.len());

	(add_shares, xor_shares)
}

}


// pub fn verify_syncing_status(ctx: &mut init::Context) {

// 	let test_ss = vec![Wrapping(
// 		if (*ctx).asymmetric_bit == 1 {
// 			0x00000000FF000000 as u64
// 		} else {
// 			0x000000000000FF00 as u64
// 		}
// 	)];

// 	let test_reveal = protocol::reveal( &test_ss, ctx, ctx.decimal_precision, false, true ); 

// 	if 0x00000000FF00FF00 != test_reveal[0] as u64 {
// 		panic!("Parties not communicating correctly");
// 	} else {
// 		println!("LR MODULE     : Parties sending correct endianness");
// 	}

// }
